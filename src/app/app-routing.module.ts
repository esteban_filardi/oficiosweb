import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomepageComponent } from './homepage/homepage.component';
import { SearchPageComponent } from './client/search-page/search-page.component';
import { TradesmanRegistrationComponent } from './tradesman/tradesman-registration/tradesman-registration.component';
import { TradesmanDetailComponent } from './client/tradesman-detail/tradesman-detail.component';
import { WriteReviewComponent } from './client/write-review/write-review.component';
import { LoginComponent } from './client/login/login.component';
import { UserConfirmAccountComponent } from './common/components/user-confirm-account/user-confirm-account.component';
import { LoginComponent as TradesmanLoginComponent } from './tradesman/login/login.component';
import { TradesmanDashboardComponent } from './tradesman/tradesman-dashboard/tradesman-dashboard.component';
import { SiteLayoutComponent } from './common/components/site-layout/site-layout.component';
import { TradesmanAreaLayoutComponent } from './common/components/tradesman-area-layout/tradesman-area-layout.component';
import { TradesmanProfileComponent } from './tradesman/tradesman-profile/tradesman-profile.component';
import { TradesmanProfileEditComponent } from './tradesman/tradesman-profile-edit/tradesman-profile-edit.component';
import { ClientRegistrationComponent } from './client/client-registration/client-registration.component';
import {TradesmanReviewsComponent} from './tradesman/tradesman-reviews/tradesman-reviews.component';
import {AdminLoginComponent} from './admin/admin-login/admin-login.component';
import {AdminDashboardComponent} from './admin/common/components/admin-dashboard/admin-dashboard.component';
import {AdminLayoutComponent} from './admin/common/components/admin-layout/admin-layout.component';
import {AdminTradesComponent} from './admin/skilled-trades/admin-trades/admin-trades.component';
import {AdminSkilledTradeEditComponent} from './admin/skilled-trades/admin-skilled-trade-edit/admin-skilled-trade-edit.component';
import {AdminSkilledTradeNewComponent} from './admin/skilled-trades/admin-skilled-trade-new/admin-skilled-trade-new.component';
import {ClientListPageComponent} from './admin/clients/client-list-page/client-list-page.component';
import {AdminClientDetailPageComponent} from './admin/clients/admin-client-detail-page/admin-client-detail-page.component';
import {AdminTradesmenListComponent} from './admin/tradesmen/admin-tradesmen-list/admin-tradesmen-list.component';
import {AdminTradesmanDetailComponent} from './admin/tradesmen/admin-tradesman-detail/admin-tradesman-detail.component';
// tslint:disable-next-line:max-line-length
import {SearchTradesmanByPersonalDataComponent} from './client/search-tradesman-by-personal-data/search-tradesman-by-personal-data.component';
import {UserNotEmailConfirmedComponent} from './common/components/user-not-email-confirmed/user-not-email-confirmed.component';
import {UpdateReviewComponent} from './client/update-review/update-review.component';

const routes: Routes = [
  {
    path: '',
    component: SiteLayoutComponent,
    children: [
      { path: '', component: HomepageComponent, pathMatch: 'full' },
      { path: 'busqueda', component: SearchPageComponent },
      { path: 'ingreso', component: LoginComponent },
      { path: 'registro', component: ClientRegistrationComponent },
      { path: 'registro-persona-de-oficio', component: TradesmanRegistrationComponent },
      { path: 'detalle/:id', component: TradesmanDetailComponent },
      { path: 'escribir-evaluacion/:id', component: WriteReviewComponent },
      { path: 'opiniones/:id', component: UpdateReviewComponent },
      { path: 'confirmar-correo-electronico', component: UserConfirmAccountComponent },
      { path: 'ingreso-trabajador', component: TradesmanLoginComponent },
      { path: 'ingreso-administrador', component:  AdminLoginComponent },
      { path: 'busqueda-por-datos-personales', component:  SearchTradesmanByPersonalDataComponent },
      { path: 'usuario-no-verificado', component:  UserNotEmailConfirmedComponent }
    ]
  },
  {
    path: 'panel-trabajador',
    component: TradesmanAreaLayoutComponent,
    children: [
      { path: '', component: TradesmanDashboardComponent, pathMatch: 'full' },
      { path: 'perfil', component: TradesmanProfileComponent },
      { path: 'editar-perfil', component: TradesmanProfileEditComponent },
      { path: 'opiniones', component: TradesmanReviewsComponent }
    ]
  },
  {
    path: 'area-administrador',
    component: AdminLayoutComponent,
    children: [
      { path: '', component: AdminDashboardComponent, pathMatch: 'full' },
      { path: 'oficios', component: AdminTradesComponent },
      { path: 'oficios/:id/editar', component: AdminSkilledTradeEditComponent },
      { path: 'oficios/nuevo', component: AdminSkilledTradeNewComponent },
      { path: 'clientes', component: ClientListPageComponent },
      { path: 'clientes/:clientId', component: AdminClientDetailPageComponent },
      { path: 'trabajadores', component: AdminTradesmenListComponent },
      { path: 'trabajadores/:tradesmanId', component: AdminTradesmanDetailComponent }
    ]
  },

  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' }) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
