import {State} from '../models/state';

export function orderCitiesOfState(states: State[]) {
  states.forEach((state) => {
    state.cities.sort((cityLeft, cityRight) => {
      return cityLeft.name.localeCompare(cityRight.name, 'es');
    });
  });
}
