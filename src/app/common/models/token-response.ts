export class TokenPayload {
    sub: string
    roles: string[]
}

export class AccessToken {
    accessToken: string;

    getTokenPayload(): TokenPayload {
        return JSON.parse((atob(this.accessToken.split(".")[1]))) as TokenPayload
    }
}
