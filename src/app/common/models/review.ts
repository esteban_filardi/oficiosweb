export class Review {
    constructor(
        public tradesmanId,
        public averageScore,
        public text,
        public createdDate: Date) {}
}
