import { Review } from './review';

export class Tradesman {
    constructor(
        public id?: number,
        public name?: string,
        public lastName?: string,
        public email?: string,
        public phoneNumber?: string,
        public city?: any,
        public tradesmanSkilledTrades: any[] = [],
        public averageScore?: number,
        public reviews?: Review[],
        public imageProfilePath?: string,
        public dateCreated?: Date,
        public profileMarkup?: string,
        public blocked?: boolean) {}
}
