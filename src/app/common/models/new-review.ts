export interface NewReview {
  tradesmanId?: number;
  text?: string;
  clientId?: number;
  qualityScores: QualityScore[];
  anonymous: boolean;
}

export interface QualityScore {
  tradesmanQualityId?: string;
  score?: number;
}
