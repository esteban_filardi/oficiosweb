export interface TradesmanQuality {
  tradesmanQualityId: string;
  label: string;
  sortOrder: number;
}
