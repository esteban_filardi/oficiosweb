import { Component, Input } from '@angular/core';

@Component({
    templateUrl: './spinner.component.html',
    selector: 'app-spinner',
    styleUrls: [
        './spinner.component.css'
    ]
})
export class SpinnerComponent {
    @Input()
    active = true;
}
