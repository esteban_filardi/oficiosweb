import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserNotEmailConfirmedComponent } from './user-not-email-confirmed.component';

describe('UserNotEmailConfirmedComponent', () => {
  let component: UserNotEmailConfirmedComponent;
  let fixture: ComponentFixture<UserNotEmailConfirmedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserNotEmailConfirmedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserNotEmailConfirmedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
