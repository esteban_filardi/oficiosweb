import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-user-not-email-confirmed',
  templateUrl: './user-not-email-confirmed.component.html',
  styleUrls: ['./user-not-email-confirmed.component.scss']
})
export class UserNotEmailConfirmedComponent implements OnInit {
  email: string;
  userType: string;
  backUrl: string;
  sendingEmail: boolean;
  emailSent: boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    private verificationEmailSenderService: UserService
  ) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params => {
      this.userType = params.get('userType');
      this.email = params.get('userEmail');

      if (this.userType === 'tradesman') {
        this.backUrl = '/ingreso-trabajador';
      } else {
        this.backUrl = '/ingreso';
      }
    });
  }

  resendVerificationEmail() {
    this.sendingEmail = true;
    this.verificationEmailSenderService
      .sendVerificationEmail(this.email, this.userType)
      .subscribe({
        next: () => {
          this.emailSent = true;
          this.sendingEmail = false;
        },
        error: error => {
          this.sendingEmail = false;
          alert('Ha ocurrido un error. Por favor, intente luego nuevamente.');
          console.error(error);
        }
      });
  }
}
