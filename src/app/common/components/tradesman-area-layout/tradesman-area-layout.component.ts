import { Component, OnDestroy, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Router } from '@angular/router';
import { TradesmanAuthService } from '../../services/auth/tradesman-auth.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './tradesman-area-layout.component.html'
})
export class TradesmanAreaLayoutComponent {
  constructor (private router: Router,
    private authService: TradesmanAuthService
  ) {}

  onLogoutClick() {
    this.authService.logoutTradesman();
    this.router.navigate(['/ingreso-trabajador']);
            return;
  }
}
