import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {UserService} from '../../services/user.service';

@Component({
    templateUrl: './user-confirm-account.component.html'
})
export class UserConfirmAccountComponent implements OnInit {
    loading = true;
    successfulConfirmation: boolean;

    constructor(
        private userService: UserService,
        private route: ActivatedRoute
    ) {}

    ngOnInit(): void {
        const userId = this.route.snapshot.queryParamMap.get('userId');
        const confirmationToken = this.route.snapshot.queryParamMap.get('confirmationToken');

        this.userService.confirmTradesman(userId, confirmationToken)
            .subscribe((response) => {
                this.loading = false;
                this.successfulConfirmation = true;
            }, (error) => {
                this.loading = false;
                this.successfulConfirmation = false;
                console.error(error);
            });
    }
}
