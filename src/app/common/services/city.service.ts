import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { City } from '../models/city';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
};

@Injectable({
    providedIn: 'root'
})
export class CityService {
    constructor(private httpClient: HttpClient) { }

    getCities(): Observable<City[]> {
        return this.httpClient.get<City[]>(environment.apiUrl + 'cities');
    }
}
