import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {map, catchError, concatAll} from 'rxjs/operators';
import {AccessToken } from '../../models/token-response';
import {environment} from 'src/environments/environment';
import { TradesmenService } from 'src/app/admin/tradesmen/common/services/tradesmen.service';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class TradesmanAuthService {
  readonly clientAuthKey = 'client-auth';
  readonly tradesmanAuthKey = 'tradesman-auth';
  readonly adminAuthKey = 'admin-auth';

  clientId = 'Oficios';

  constructor(
    private authenticationService: AuthenticationService,
    private http: HttpClient,
    private tradesmenService: TradesmenService) {
  }

  loginTradesman(email: string, password: string): Observable<boolean> {
    const url = environment.apiUrl + 'tokens';
    const data = {
      username: email,
      password,
      role: "Tradesman"
    };

    return this.getAuthFromServer(url, data);
  }

  getAuthFromServer(url: string, data: any): Observable<boolean> {
    let accessToken = null;
    return this.http.post<AccessToken>(url, data)
      .pipe(map(response => {   
        var tokenResponse = Object.assign(new AccessToken(), response);
        
        accessToken = tokenResponse.accessToken;
        this.authenticationService.storeAccessToken(accessToken);
        
        return tokenResponse.getTokenPayload();
      }), map(tokenPayload => {
        return this.tradesmenService.getTradesmanByUserId(tokenPayload.sub);        
      }),
      concatAll(),
      map(tradesman => {
        console.log(tradesman);
        this.removeOtherAuthorizationData();
        // store username and jwt token
        const userData = {
          token: accessToken,
          tradesmanId: tradesman.id,
          first_name: tradesman.name,
          last_name: tradesman.lastName
        }

        this.setAuthorizationData(userData);

        return true;
      })
      , catchError(error => {
        console.error(error);
        return throwError(error);
      }));
  }

  logout(): boolean {
    this.setAuthorizationData(null);
    return true;
  }

  setAuthorizationData(auth: object): boolean {
    if (auth) {
      localStorage.setItem(
        this.tradesmanAuthKey,
        JSON.stringify(auth));
    } else {
      localStorage.removeItem(this.tradesmanAuthKey);
    }

    return true;
  }

  getAuth(): any {
    const i = localStorage.getItem(this.tradesmanAuthKey);
    if (i) {
      return JSON.parse(i);
    }

    return null;
  }

  isLoggedIn(): boolean {
    return localStorage.getItem(this.tradesmanAuthKey) != null;
  }

  getLoggedTradesmanId(): number {
    const auth = this.getAuth();
    if (!auth) {
      return null;
    }

    return auth.tradesmanId;
  }

  logoutTradesman(): boolean {
    this.setAuthorizationData(null);
    return true;
  }

  private removeOtherAuthorizationData() {
    localStorage.removeItem(this.clientAuthKey);
    localStorage.removeItem(this.adminAuthKey);
  }
}
