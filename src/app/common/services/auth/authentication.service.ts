import { Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {
    private readonly accessTokenKey = 'accessToken';    

    storeAccessToken(token: string): void {
        localStorage.setItem(this.accessTokenKey, token);
    }

    getAccessToken(): string {
        return localStorage.getItem(this.accessTokenKey);
    }
}
