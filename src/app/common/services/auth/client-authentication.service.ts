import {Injectable} from '@angular/core';
import {Observable, Subject, throwError} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {AccessToken} from '../../models/token-response';
import {catchError, concatAll, map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import { ClientService } from '../client.service';
import {AuthenticationService} from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class ClientAuthenticationService {
  readonly clientAuthKey = 'client-auth';
  readonly tradesmanAuthKey = 'tradesman-auth';
  readonly adminAuthKey = 'admin-auth';
  readonly clientId = 'Oficios';

  private loginSource = new Subject<any>();
  private logoutSource = new Subject<any>();

  loginSource$ = this.loginSource.asObservable();
  logoutSource$ = this.logoutSource.asObservable();

  constructor(
    private authenticationService: AuthenticationService,
    private httpClient: HttpClient,
    private clientsService: ClientService
  ) {
  }

  login(email: string, password: string): Observable<boolean> {
    const url = environment.apiUrl + 'tokens';
    const data = {
      username: email,
      password,
      role: "Client"
    };

    let accessToken = null;
    return this.httpClient.post<AccessToken>(url, data)
      .pipe(map(response => {   
        var tokenResponse = Object.assign(new AccessToken(), response);
        
        accessToken = tokenResponse.accessToken;
        this.authenticationService.storeAccessToken(accessToken);

        return tokenResponse.getTokenPayload();
      }), map(tokenPayload => {
        return this.clientsService.getClientByUserId(tokenPayload.sub);        
      }),
      concatAll(),
      map(client => {
        this.removeOtherAuthorizationData();
        // store username and jwt token
        const userData = {
          token: accessToken,
          clientId: client.id,
          first_name: client.firstName,
          last_name: client.lastName
        }
        this.setAuthorizationData(userData);
        this.loginSource.next(userData);

        
        return true;
      })
      , catchError(error => {
        console.error(error);
        return throwError(error);
      }));
  }  
  
  setAuthorizationData(auth: any): boolean {
    if (auth) {
      localStorage.setItem(
        this.clientAuthKey,
        JSON.stringify(auth));
    } else {
      localStorage.removeItem(this.clientAuthKey);
    }

    return true;
  }

  loginUsingFacebook(loginData: AccessToken) {
    this.authenticationService.storeAccessToken(loginData.accessToken);

    this.clientsService.getClientByUserId(loginData.getTokenPayload().sub).subscribe(client => {
      this.removeOtherAuthorizationData();
      // store username and jwt token
      const userData = {
        token: loginData.accessToken,
        clientId: client.id,
        first_name: client.firstName,
        last_name: client.lastName
      }

      this.setAuthorizationData(userData);
      this.loginSource.next(userData);
    })    
  }

  logout() {
    localStorage.removeItem((this.clientAuthKey));
    this.loginSource.next();
  }

  getLoggedClient() {
    return JSON.parse(localStorage.getItem(this.clientAuthKey));
  }

  private removeOtherAuthorizationData() {
    localStorage.removeItem(this.tradesmanAuthKey);
    localStorage.removeItem(this.adminAuthKey);
  }

  isLoggedIn(): boolean {
    return localStorage.getItem(this.clientAuthKey) != null;
  }
}
