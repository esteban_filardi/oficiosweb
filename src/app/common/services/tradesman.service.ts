import {HttpClient, HttpHeaders, HttpParams, HttpResponse} from '@angular/common/http';
import { Tradesman } from '../models/tradesman';
import { Tradesman as TradesmanForAdmin } from '../../admin/common/models/tradesman';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Review } from '../models/review';
import { environment } from 'src/environments/environment';
import { TradesmanRegistration } from 'src/app/tradesman/tradesman-registration/tradesman-registration';
import {NewReview} from '../models/new-review';
import {map} from 'rxjs/operators';
import {SkilledTrade} from '../../admin/skilled-trades/admin-trades/models/skilled-trade';
import {SearchModel} from '../../client/search-tradesman-by-personal-data/search-model';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
    providedIn: 'root',
})
export class TradesmanService {
    constructor(
        private http: HttpClient) { }


    createHttpParams(params: {}): HttpParams {
        let httpParams: HttpParams = new HttpParams();
        Object.keys(params).forEach(param => {
          if (params[param]) {
            httpParams = httpParams.set(param, params[param]);
          }
        });

        return httpParams;
    }

    getTradesmen(searchModel: SearchModel, page = 1): Observable<HttpResponse<Tradesman[]>> {
        let searchParameters = this.createHttpParams(searchModel);
        searchParameters = searchParameters.set('page', String(page));
        return this.http.get<Tradesman[]>(environment.apiUrl + 'tradesmen', {
            observe: 'response',
            params: searchParameters
        });
    }

    getTradesman(id: number): Observable<Tradesman> {
        return this.http.get<Tradesman>(environment.apiUrl + 'tradesmen/' + id);
    }

    registerTradesman(tradesman: TradesmanRegistration): Observable<Tradesman> {
        const postUrl = environment.apiUrl + 'tradesmanRegistrations';
        return this.http.post<Tradesman>(postUrl, tradesman, httpOptions);
    }

    addReview(review: NewReview): Observable<Review>   {
        const postUrl = environment.apiUrl + 'tradesmen/' + review.tradesmanId + '/reviews';
        return this.http.post<Review>(postUrl, review, httpOptions);
    }

    updateProfile(tradesmanId, tradesmanUpdatedValues): Observable<any> {
        const putUrl = environment.apiUrl + 'tradesmen/' + tradesmanId;
        return this.http.put(putUrl, tradesmanUpdatedValues, httpOptions);
    }
}
