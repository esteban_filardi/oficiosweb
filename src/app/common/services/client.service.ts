import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ClientRegistrationDto } from 'src/app/client/client-registration/client-registration-dto';
import {map} from 'rxjs/operators';
import {Client} from '../../admin/clients/common/models/client';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
    providedIn: 'root',
})
export class ClientService {
    constructor(
        private http: HttpClient) { }

    registerClient(clientRegistrationDto: ClientRegistrationDto): Observable<any> {
        const postUrl = environment.apiUrl + 'clients';
        return this.http.post(postUrl, clientRegistrationDto, httpOptions);
    }

  getClients(): Observable<Client[]> {
    const methodUrl = `${environment.apiUrl}clients`;
    return map((response: Client[]) => {
      return response;
    })(this.http.get<Client[]>((methodUrl)));
  }

  getClientByUserId(userId: string) : Observable<Client> {
    const methodUrl = `${environment.apiUrl}clients`;

    return this.http.get<Client[]>(methodUrl, {
      params: {
        userId
      }
    }).pipe(map(clients => {
      return clients.length > 0 ? clients[0] : null;
    }));
  }

  getClient(clientId: string) : Observable<Client> {
    const methodUrl = `${environment.apiUrl}clients/${clientId}`;
    return map((response: Client) => {
      return response;
    })(this.http.get<Client>((methodUrl)));
  }

  blockClient(clientId: number): Observable<void> {
    const methodUrl = `${environment.apiUrl}clients/${clientId}/blocked`;

    return this.http.put<void>(methodUrl, {});
  }
}
