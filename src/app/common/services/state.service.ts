import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { State } from '../models/state';
import { environment } from 'src/environments/environment';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
};

@Injectable({
    providedIn: 'root'
})
export class StateService {
    private apiUrl: string;

    constructor(private httpClient: HttpClient) {
      this.apiUrl = environment.apiUrl;
    }

    getStates(): Observable<State[]> {
        return this.httpClient.get<State[]>(environment.apiUrl + 'states');
    }
}
