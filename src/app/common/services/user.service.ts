import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) {
  }

  sendVerificationEmail(email, userType) {
    const methodUrl = `${environment.apiUrl}users/verificationEmails`;
    return this.httpClient.post(methodUrl, {
      email,
      userType
    });
  }

  confirmTradesman(userId: string, confirmationToken: string): Observable<object> {
    const postUrl = environment.apiUrl + 'users/' + userId + '/verify';
    return this.httpClient.post(postUrl, {confirmationToken});
  }
}
