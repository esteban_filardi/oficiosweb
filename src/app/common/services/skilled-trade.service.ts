import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SkilledTrade } from '../models/skilled-trade';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
};

@Injectable({
    providedIn: 'root'
})
export class SkilledTradeService {
    constructor(private httpClient: HttpClient) { }

    getSkilledTrades(): Observable<SkilledTrade[]> {
        return this.httpClient.get<SkilledTrade[]>(environment.apiUrl + 'skilledTrades');
    }
}
