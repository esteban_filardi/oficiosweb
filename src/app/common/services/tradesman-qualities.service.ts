import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import { TradesmanQuality } from '../models/tradesman-quality';

const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class TradesmanQualitiesService {

  constructor(private httpClient: HttpClient) {
  }

  getTradesmanQualities(): Observable<TradesmanQuality[]> {
    const methodUrl = apiUrl + 'tradesmanQualities';
    return this.httpClient.get<TradesmanQuality[]>(methodUrl);
  }
}
