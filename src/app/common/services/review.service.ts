import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {Review} from '../../admin/clients/admin-client-detail-page/review';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {
  constructor(private httpClient: HttpClient) { }

  getReviewsOfClient(clientId, tradesmanId?: string | number): Observable<Review[]> {
    const options = {
      params: new HttpParams().set('clientId', clientId)
    };

    if (typeof tradesmanId  === 'number') {
      tradesmanId = String(tradesmanId);
    }

    if (tradesmanId) {
      options.params = options.params.set('tradesmanId', tradesmanId);
    }

    return this.httpClient.get<Review[]>(environment.apiUrl + 'reviews', options);
  }

  deleteReview(id: string) {
    const methodUrl = `${environment.apiUrl}reviews/${id}`;
    return map((response) => {
      return response;
    })(this.httpClient.delete(methodUrl));
  }

  getReview(reviewId: number): Observable<any>{
    const methodUrl = `${environment.apiUrl}reviews/${reviewId}`;

    return map((review) => {
      return review;
    })(this.httpClient.get(methodUrl));
  }

  updateReview(reviewId, reviewData: {text: string; reviewId: string; qualityScores: any[]}) {
    const methodUrl = `${environment.apiUrl}reviews/${reviewId}`;
    return map((response) => {
      return response;
    })(this.httpClient.put(methodUrl, reviewData));
  }

  getTradesmanReviewsForClient(tradesmanId: number) {
    const methodUrl = `${environment.apiUrl}tradesmen/${tradesmanId}/reviews`;


    return this.httpClient.get(methodUrl);
  }
}
