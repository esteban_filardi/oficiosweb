import { Component, OnInit } from '@angular/core';
import { ClientAuthenticationService } from '../services/auth/client-authentication.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html'
})
export class NavbarComponent implements OnInit {
  loggedClient: any = null;
  isCollapsed = true;

  constructor(private clientAuthenticationService: ClientAuthenticationService) {
  }

  ngOnInit() {
    this.loggedClient = this.clientAuthenticationService.getLoggedClient();

    console.log('The logged client', this.loggedClient);
    this.clientAuthenticationService.loginSource$.subscribe((loggedClient) => {
      console.log('in the subscription');
      this.loggedClient = loggedClient;
    });
  }

  clientIsLogged(): boolean {
    return !!this.loggedClient;
  }

  logout() {
    this.isCollapsed = true;
    this.clientAuthenticationService.logout();
  }

  collapseNavbar() {
    this.isCollapsed = true;
  }
}
