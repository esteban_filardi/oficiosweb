import { Component, OnInit } from '@angular/core';
import { TradesmanService } from 'src/app/common/services/tradesman.service';
import { Tradesman } from 'src/app/common/models/tradesman';
import { ActivatedRoute, Router } from '@angular/router';
import { TradesmanAuthService } from 'src/app/common/services/auth/tradesman-auth.service';
import { environment } from 'src/environments/environment';


@Component({
    templateUrl: 'tradesman-profile.component.html'
})
export class TradesmanProfileComponent implements OnInit {
    tradesman: Tradesman;
    notification = null;
    apiUrl: string;

    constructor(
        private route: ActivatedRoute,
        private tradesmanService: TradesmanService,
        private authService: TradesmanAuthService,
        private router: Router
    ) {
        this.apiUrl = environment.apiUrl;
    }

    ngOnInit(): void {
        const loggedTradesmanId = this.authService.getLoggedTradesmanId();

        if (!loggedTradesmanId) {
            this.router.navigate(['/ingreso-trabajador']);
            return;
        }

        if (this.route.snapshot.paramMap.get('notification_type')) {
            this.notification = {
                type: this.route.snapshot.paramMap.get('notification_type'),
                message: this.route.snapshot.paramMap.get('notification_message'),
            };
            console.log(this.notification);
        }
        this.tradesmanService.getTradesman(loggedTradesmanId)
            .subscribe(tradesman => this.tradesman = tradesman);
    }
}

