export interface Base64EncodedProfileImage {
    filename: string;
    fileType: string;
    base64encodedImage: string;
}
