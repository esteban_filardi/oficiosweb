import { Base64EncodedProfileImage } from './base64-encoded-profile-image';

export interface TradesmanUpdateDto {
    firstName?: string;
    lastName?: string;
    email?: string;
    phoneNumber?: string;
    cityId?: number;
    skilledTradeIds?: number[];
    profileImage: Base64EncodedProfileImage;
    profileMarkup: string;
}
