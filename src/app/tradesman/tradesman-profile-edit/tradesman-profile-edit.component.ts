import { Component, OnInit, ViewChild } from '@angular/core';
import { TradesmanService } from 'src/app/common/services/tradesman.service';
import { Tradesman } from 'src/app/common/models/tradesman';
import { ActivatedRoute, Router } from '@angular/router';
import { TradesmanAuthService } from 'src/app/common/services/auth/tradesman-auth.service';
import { StateService } from 'src/app/common/services/state.service';
import { State } from 'src/app/common/models/state';
import { City } from 'src/app/common/models/city';
import { TradesmanUpdateDto } from './tradesman-update-dto';
import { Observable, of, forkJoin } from 'rxjs';
import { SkilledTradeService } from 'src/app/common/services/skilled-trade.service';
import { map } from 'rxjs/operators';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import '@ckeditor/ckeditor5-build-classic/build/translations/es';
import {CkEditorUploadAdapterService} from './ck-editor-upload-adapter.service';

import { environment } from 'src/environments/environment';
import { SkilledTrade } from 'src/app/common/models/skilled-trade';

@Component({
    templateUrl: 'tradesman-profile-edit.component.html'
})
export class TradesmanProfileEditComponent implements OnInit {
    tradesman: Tradesman;
    tradesmanUpdateDto: TradesmanUpdateDto;
    stateId: number = null;

    states: State[];
    cities: City[];
    skilledTrades$: Observable<SkilledTrade[]>;

    profileImageUrl: string;

    @ViewChild('updateProfileForm') updateProfileForm: { form: { valid: any; }; };

    Editor = ClassicEditor;
    editorConfig = {
        language: 'es',
        extraPlugins: [

        ]
    };

    submitting = false;
    phoneNumberMask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
    hasFinishedLoading = false;

    constructor(
        private route: ActivatedRoute,
        private tradesmanService: TradesmanService,
        private authService: TradesmanAuthService,
        private router: Router,
        private stateService: StateService,
        private skilledTradeService: SkilledTradeService
    ) { }

    ngOnInit(): void {
        const loggedTradesmanId = this.authService.getLoggedTradesmanId();

        if (!loggedTradesmanId) {
            this.router.navigate(['/ingreso-trabajador']);
            return;
        }

        this.skilledTrades$ = this.skilledTradeService.getSkilledTrades().pipe(map((skilledTrades) => {
            const options = skilledTrades;
            return options;
        }));

        forkJoin(
            this.stateService.getStates(),
            this.tradesmanService.getTradesman(loggedTradesmanId)
        ).subscribe(values => {
            this.states = values[0];
            const tradesman = this.tradesman = values[1];

            this.stateId = tradesman.city.state.id;

            if (this.stateId) {
                this.getStateCities(this.stateId).subscribe(cities => this.cities = cities);
            }

            this.editorConfig.extraPlugins.push(
                // Don't use arrow function because it isn't working
                // with the current version of CKEditor. Try again with future newer versoins
                function (editor)  {
                  editor.plugins.get( 'FileRepository' ).createUploadAdapter = (loader) => {
                        return new CkEditorUploadAdapterService(loader, environment.apiUrl, tradesman.id);
                    };
                }
            );

            this.tradesmanUpdateDto = {
                firstName: tradesman.name,
                lastName: tradesman.lastName,
                phoneNumber: tradesman.phoneNumber,
                cityId: tradesman.city.id,
                skilledTradeIds: tradesman.tradesmanSkilledTrades.map(tradesmanSkilledTrade => tradesmanSkilledTrade.skilledTrade.id),
                profileImage: null,
                email: tradesman.email,
                profileMarkup: tradesman.profileMarkup
            };

            this.profileImageUrl = tradesman.imageProfilePath ? environment.apiUrl + '/uploads/' + tradesman.imageProfilePath : null;

            this.hasFinishedLoading = true;
        });
    }

    getStateCities(stateId: number): Observable<City[]> {
        for (const state of this.states) {
            if (state.id === stateId) {
                return of(state.cities);
            }
        }

        return of([]);
    }

    onStateChange() {
        this.getStateCities(this.stateId).subscribe(cities => {
            this.cities = cities;
            this.tradesmanUpdateDto.cityId = null;
        });
    }

    onSelectFile(event: Event) { // called each time file input changes
        const target = event.target as HTMLInputElement;

        if (target.files && target.files[0]) {
            const reader = new FileReader();

            const file = target.files[0];

            reader.readAsDataURL(file); // read file as data url

            reader.onload = () => {
                this.profileImageUrl = reader.result.toString();
                this.tradesmanUpdateDto.profileImage = {
                    filename: file.name,
                    fileType: file.type,
                    base64encodedImage: this.profileImageUrl.split(',')[1]
                };
            };
        }
    }

    onSubmit() {
        if (!this.updateProfileForm.form.valid) {
            return;
        }

        this.submitting = true;

        this.tradesmanService.updateProfile(this.tradesman.id, this.tradesmanUpdateDto)
            .subscribe(() => {
                this.router.navigate(['/panel-trabajador/perfil',
                    {notification_type: 'success', notification_message: 'Su perfil ha sido actualizado.'}], {
                });
                this.submitting = false;
            }, err => {
                console.log(err);
                this.submitting = false;
            });
    }
}
