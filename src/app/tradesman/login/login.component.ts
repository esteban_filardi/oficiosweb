import { Component, OnInit, ViewChild } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { Location } from '@angular/common';
import { TradesmanAuthService } from 'src/app/common/services/auth/tradesman-auth.service';
import { LoginDataDto } from './login-data-dto';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  @ViewChild('loginForm') loginForm: { form: { valid: any; }; };
  loginData: LoginDataDto = new LoginDataDto();
  submitting = false;
  invalidCredentials = false;
  userIsBlocked = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private authService: TradesmanAuthService
  ) {}

  ngOnInit() {}

  onLogin() {
    if (!this.loginForm.form.valid) {
      return;
    }

    this.invalidCredentials = false;
    this.userIsBlocked = false;
    this.submitting = true;
    const username = this.loginData.email;
    const password = this.loginData.password;

    this.authService.loginTradesman(username, password)
      .subscribe(
        res => {
          console.log('Logging success');
          this.submitting = false;
          this.router.navigate(['/panel-trabajador']);
        },
        (err: HttpErrorResponse) => {

          this.submitting = false;

          if (err.status === 401) {
            this.invalidCredentials = true;
            return;
          }

          if (err.status === 403 && err.error.type === 'userIsBlocked') {
            this.userIsBlocked = true;
            return;
          }

          if (err.status === 403 && err.error.type === 'userIsNotEmailConfirmed') {
            this.router.navigate(['/usuario-no-verificado', {
              userType: 'tradesman',
              userEmail: this.loginData.email
            }]);
          }

          console.log('Logging error', err);
        }
      );
  }
}
