import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TradesmanService } from 'src/app/common/services/tradesman.service';
import { Tradesman } from 'src/app/common/models/tradesman';
import { TradesmanAuthService } from 'src/app/common/services/auth/tradesman-auth.service';

@Component({
    templateUrl: 'tradesman-dashboard.component.html'
})
export class TradesmanDashboardComponent implements OnInit {
    tradesman: Tradesman;

    constructor(
        private route: ActivatedRoute,
        private tradesmanService: TradesmanService,
        private authService: TradesmanAuthService,
        private router: Router
    ) { }

    ngOnInit(): void {
        const loggedTradesmanId = this.authService.getLoggedTradesmanId();

        if (!loggedTradesmanId) {
            this.router.navigate(['/ingreso-trabajador']);
            return;
        }

        this.tradesmanService.getTradesman(loggedTradesmanId)
            .subscribe(tradesman => this.tradesman = tradesman);
    }
}
