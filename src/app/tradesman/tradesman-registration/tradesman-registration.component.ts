import { Component, OnInit, ViewChild } from '@angular/core';
import { TradesmanService } from '../../common/services/tradesman.service';
import { CityService } from '../../common/services/city.service';
import { SkilledTradeService } from '../../common/services/skilled-trade.service';
import { City } from '../../common/models/city';
import { map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { TradesmanRegistration } from './tradesman-registration';
import { StateService } from 'src/app/common/services/state.service';
import { State } from 'src/app/common/models/state';
import { SkilledTrade } from 'src/app/common/models/skilled-trade';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
    templateUrl: './tradesman-registration.component.html'
})
export class TradesmanRegistrationComponent implements OnInit {
    submitted = false;
    isSubmitting = false;
    successfulRegistration = false;
    skilledTrades$: Observable<SkilledTrade[]>;

    stateId: number;
    states: State[] = [];
    cities: City[] = [];

    @ViewChild('tradesmanRegistrationForm') tradesmanRegistrationForm: { form: { valid: any; }; };
    model: TradesmanRegistration = new TradesmanRegistration();
    submitting = false;
    phoneNumberMask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

    constructor(
        private tradesmanService: TradesmanService,
        private cityService: CityService,
        private skilledTradeService: SkilledTradeService,
        private stateService: StateService
    ) { }

    ngOnInit(): void {
        this.skilledTrades$ = this.skilledTradeService.getSkilledTrades().pipe(map((skilledTrades) => {
            const options = skilledTrades;
            return options;
        }));

        this.stateService.getStates().subscribe(states => this.states = states);
    }

    getStateCities(stateId: number): Observable<City[]> {
        for (const state of this.states) {
            if (state.id === stateId) {
                return of(state.cities);
            }
        }

        return of([]);
    }

    onStateChange() {
        this.getStateCities(this.stateId).subscribe(cities => {
            this.cities = cities;
            this.model.cityId = null;
        });
    }

    onSubmit() {
        if (!this.tradesmanRegistrationForm.form.valid) {
            return;
        }

        this.submitting = true;

        this.tradesmanService.registerTradesman(this.model)
            .subscribe(tradesman => {
                this.successfulRegistration = true;
                this.submitting = false;
                window.scroll(window.scrollX, 0);
            }, (error) => {
                if (error instanceof HttpErrorResponse && error.status === 409) {
                    if (error.status === 409) {
                      alert('El correo electrónico ingresado ya se encuentra en uso.');
                    }
                } else {
                    alert('Ha ocurrido un error. Por favor, intente luego nuevamente.');
                    console.error('An error has occurred', error);
                }

                this.submitting = false;
            });
    }
}
