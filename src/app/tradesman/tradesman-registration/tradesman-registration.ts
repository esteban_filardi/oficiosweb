export class TradesmanRegistration {
    constructor(
        public id?: number,
        public firstName?: string,
        public lastName?: string,
        public cityId?,
        public skilledTradeIds?,
        public phoneNumber?,
        public email?,
        public password?,
        public passwordConfirm?,
    ) {}
}
