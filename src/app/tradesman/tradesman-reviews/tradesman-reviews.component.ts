import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import {ActivatedRoute, Router} from '@angular/router';
import {TradesmanService} from '../../common/services/tradesman.service';
import {TradesmanAuthService} from '../../common/services/auth/tradesman-auth.service';
import {Tradesman} from '../../common/models/tradesman';
import { forkJoin } from 'rxjs';
import { ReviewService } from 'src/app/common/services/review.service';

@Component({
  selector: 'app-tradesman-reviews',
  templateUrl: './tradesman-reviews.component.html',
  styleUrls: ['./tradesman-reviews.component.css']
})
export class TradesmanReviewsComponent implements OnInit {
  tradesman: Tradesman;
  apiUrl: string;
  reviews: Object;

  constructor(
    private route: ActivatedRoute,
    private tradesmanService: TradesmanService,
    private authService: TradesmanAuthService,
    private router: Router,
    private reviewService: ReviewService,
  ) {
    this.apiUrl = environment.apiUrl;
  }

  ngOnInit(): void {
    const loggedTradesmanId = this.authService.getLoggedTradesmanId();

    if (!loggedTradesmanId) {
      this.router.navigate(['/ingreso-trabajador']);
      return;
    }

    forkJoin([this.tradesmanService.getTradesman(loggedTradesmanId),
      this.reviewService.getTradesmanReviewsForClient(loggedTradesmanId)]).subscribe(([tradesman, reviews]) => {
        this.tradesman = tradesman;
        this.reviews = reviews;
      })
  }
}
