import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TradesmanReviewsComponent } from './tradesman-reviews.component';

describe('TradesmanReviewsComponent', () => {
  let component: TradesmanReviewsComponent;
  let fixture: ComponentFixture<TradesmanReviewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TradesmanReviewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TradesmanReviewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
