import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TradesmanService} from '../../common/services/tradesman.service';
import {ClientAuthenticationService} from '../../common/services/auth/client-authentication.service';
import {ReviewService} from '../../common/services/review.service';
import {TradesmanQualitiesService} from '../../common/services/tradesman-qualities.service';
import {NewReview, QualityScore} from '../../common/models/new-review';
import {forkJoin} from 'rxjs';
import {Tradesman} from '../../common/models/tradesman';
import { TradesmanQuality } from 'src/app/common/models/tradesman-quality';

@Component({
  selector: 'app-view-review',
  templateUrl: './update-review.component.html',
  styleUrls: ['./update-review.component.scss']
})
export class UpdateReviewComponent implements OnInit {
  review: any;
  model = {
    reviewId: null,
    text: '',
    qualityScores: [],
    anonymous: true
  };
  tradesmanQualities: TradesmanQuality[];

  submitting = false;
  @ViewChild('writeReviewForm') writeReviewForm: { form: { valid: any; }; };
  hasFinishedLoading: boolean;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authService: ClientAuthenticationService,
    private tradesmanService: TradesmanService,
    private clientAuthenticationService: ClientAuthenticationService,
    private reviewService: ReviewService,
    private tradesmanQualitiesService: TradesmanQualitiesService
  ) {

  }

  ngOnInit() {
    const reviewId = +this.route.snapshot.paramMap.get('id');


    this.redirectIfClientIsNotloggedIn();

    const id = +this.route.snapshot.paramMap.get('id');

    const clientId = this.getLoggedClientId();

    forkJoin([
      this.reviewService.getReview(reviewId),
      this.tradesmanQualitiesService.getTradesmanQualities()
    ]).subscribe(([review, tradesmanQualities]) => {
      this.model.reviewId = reviewId;
      this.model.text = review.text;
      this.model.anonymous = review.anonymous;
      this.review = review;
      this.tradesmanQualities = tradesmanQualities;

      this.preloadTradesmanQualityScoresModel(tradesmanQualities, review);
      this.hasFinishedLoading = true;
    });
  }

  redirectIfClientIsNotloggedIn(): void {
    if (this.authService.isLoggedIn()) {
      return;
    }

    this.router.navigate(['/ingreso'], {
      queryParams: {
        returnUrl: this.router.url,
        message: 'Para poder realizar una evaluación debe ingresar al sitio.'
      },
      replaceUrl: true
    });
  }

  private preloadTradesmanQualityScoresModel(tradesmanQualities: TradesmanQuality[], review) {
    this.model.qualityScores = tradesmanQualities.map(t => {
      const currentQualityScore = review.qualityScores.find(qualityScore => {
        return qualityScore.tradesmanQuality.tradesmanQualityId ===  t.tradesmanQualityId;
      });

      return {
        tradesmanQualityId: t.tradesmanQualityId,
        score: currentQualityScore ? currentQualityScore.score : null
      } as QualityScore;
    });
  }

  private getLoggedClientId() {
    const loggedClient = this.authService.getLoggedClient();
    return loggedClient.clientId;
  }

  onSubmit()  {
    if (!this.writeReviewForm.form.valid) {
      return;
    }

    this.submitting = true;

    this.reviewService.updateReview(this.model.reviewId, this.model)
      .subscribe(review => {
        this.router.navigate(['/detalle/' + this.review.tradesman.tradesmanId]);
        this.submitting = false;
      }, err => {
        console.log(err);
        this.submitting = false;
      });
  }
}
