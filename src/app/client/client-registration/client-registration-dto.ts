export interface ClientRegistrationDto {
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    passwordConfirm: string;
}
