import { Component, OnInit, ViewChild } from '@angular/core';
import { ClientRegistrationDto } from './client-registration-dto';
import { ClientService } from 'src/app/common/services/client.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  templateUrl: 'client-registration.component.html'
})
export class ClientRegistrationComponent implements OnInit {
  submitted = false;
  isSubmitting = false;
  successfulRegistration = false;

  @ViewChild('clientRegistrationForm') clientRegistrationForm: {
    form: { valid: any };
  };
  clientRegistrationDto: ClientRegistrationDto = {
    email: '',
    firstName: '',
    lastName: '',
    password: '',
    passwordConfirm: ''
  };
  submitting = false;
  phoneNumberMask = [
    '(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/
  ];

  constructor(private clientService: ClientService) {}

  ngOnInit(): void {}

  onSubmit() {
    if (!this.clientRegistrationForm.form.valid) {
      return;
    }

    this.submitting = true;

    this.clientService.registerClient(this.clientRegistrationDto).subscribe(
      client => {
        this.successfulRegistration = true;
        this.submitting = false;
        window.scroll(window.scrollX, 0);
      },
      error => {
        if (error instanceof HttpErrorResponse && error.status === 409) {
          if (error.status === 409) {
            alert('El correo electrónico ingresado ya se encuentra en uso.');
          }
        } else {
          alert('Ha ocurrido un error. Por favor, intente luego nuevamente.');
          console.error('An error has occurred', error);
        }

        this.submitting = false;
      }
    );
  }
}
