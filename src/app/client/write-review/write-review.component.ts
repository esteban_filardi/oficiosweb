import { Component, OnInit, ViewChild } from '@angular/core';
import { TradesmanService } from '../../common/services/tradesman.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Tradesman } from '../../common/models/tradesman';
import {NewReview, QualityScore} from '../../common/models/new-review';
import {ClientAuthenticationService} from '../../common/services/auth/client-authentication.service';
import {TradesmanQualitiesService} from '../../common/services/tradesman-qualities.service';
import {forkJoin} from 'rxjs';
import { TradesmanQuality } from 'src/app/common/models/tradesman-quality';

@Component({
  templateUrl: './write-review.component.html',
  styleUrls: ['./write-review.component.css']
})
export class WriteReviewComponent implements OnInit {
  tradesman: Tradesman;
  model: NewReview = {
    tradesmanId: null,
    text: null,
    qualityScores: [],
    anonymous: true
  };

  submitting = false;
  @ViewChild('writeReviewForm') writeReviewForm: { form: { valid: any; }; };
  tradesmanQualities: TradesmanQuality[];

  constructor(
    private authService: ClientAuthenticationService,
    private router: Router,
    private route: ActivatedRoute,
    private tradesmanService: TradesmanService,
    private tradesmanQualitiesService: TradesmanQualitiesService
  ) {
  }

  ngOnInit() {
    this.redirectIfClientIsNotloggedIn();

    const id = +this.route.snapshot.paramMap.get('id');

    this.model.clientId = this.getLoggedClientId();

    forkJoin([
      this.tradesmanService.getTradesman(id),
      this.tradesmanQualitiesService.getTradesmanQualities()
    ]).subscribe(([tradesman, tradesmanQualities]) => {
      this.tradesman = tradesman;
      this.model.tradesmanId = this.tradesman.id;
      this.tradesmanQualities = tradesmanQualities;

      this.preloadTradesmanQualityScoresModel(tradesmanQualities);
    });
  }

  redirectIfClientIsNotloggedIn(): void {
    if (this.authService.isLoggedIn()) {
      return;
    }

    this.router.navigate(['/ingreso'], {
      queryParams: {
        returnUrl: this.router.url,
        message: 'Para poder realizar una evaluación debe ingresar al sitio.'
      },
      replaceUrl: true
    });
  }

  onSubmit()  {
    if (!this.writeReviewForm.form.valid) {
      return;
    }

    this.submitting = true;

    this.tradesmanService.addReview(this.model)
      .subscribe(review => {
        this.router.navigate(['/detalle/' + this.tradesman.id]);
        this.submitting = false;
      }, err => {
        console.log(err);
        this.submitting = false;
      });
  }

   getLoggedClientId() {
     const loggedClient = this.authService.getLoggedClient();
     return loggedClient.clientId;
  }

  private preloadTradesmanQualityScoresModel(tradesmanQualities: TradesmanQuality[]) {
    this.model.qualityScores = tradesmanQualities.map(t => {
      return {
        tradesmanQualityId: t.tradesmanQualityId
      } as QualityScore;
    });
  }
}
