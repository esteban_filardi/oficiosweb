import {State} from '../../common/models/state';

export interface SearchModel {
  skilledTradeId?: string;
  firstName?: string;
  lastName?: string;
  email?: string;
  phone?: string;
  state?: State;
  cityId?: string;
}
