import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchTradesmanByPersonalDataComponent } from './search-tradesman-by-personal-data.component';

describe('SearchTradesmanByPersonalDataComponent', () => {
  let component: SearchTradesmanByPersonalDataComponent;
  let fixture: ComponentFixture<SearchTradesmanByPersonalDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchTradesmanByPersonalDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchTradesmanByPersonalDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
