import {Component, OnInit, ViewChild} from '@angular/core';
import {forkJoin} from 'rxjs';
import {orderCitiesOfState} from '../../common/functions/order-cities-of-state';
import {State} from '../../common/models/state';
import {SkilledTrade} from '../../common/models/skilled-trade';
import {Tradesman} from '../../common/models/tradesman';
import {TradesmanService} from '../../common/services/tradesman.service';
import {SkilledTradeService} from '../../common/services/skilled-trade.service';
import {StateService} from '../../common/services/state.service';
import {ActivatedRoute} from '@angular/router';
import {environment} from '../../../environments/environment';
import {SearchModel} from './search-model';

@Component({
  selector: 'app-search-tradesman-by-personal-data',
  templateUrl: './search-tradesman-by-personal-data.component.html',
  styleUrls: ['./search-tradesman-by-personal-data.component.scss']
})
export class SearchTradesmanByPersonalDataComponent implements OnInit {
  searchBy = 'nameAndCity';

  statesOptions: State[];

  searchModel: SearchModel = {};

  tradesmen: Tradesman[] = [];

  searchSettingsReady = false;
  resultsReady = false;

  page: number;
  totalNumberOfResults: number;
  showNoResultsMessage = false;

  @ViewChild('searchForm') searchForm: { form: { valid: any; }; };
  apiUrl: string;

  constructor(
    private tradesmanService: TradesmanService,
    private skilledTradeService: SkilledTradeService,
    private stateService: StateService,
    private route: ActivatedRoute
  ) {
    this.apiUrl = environment.apiUrl;
  }

  ngOnInit(): void {
    this.searchModel.cityId = this.route.snapshot.queryParamMap.get('localidadId');

    forkJoin(this.stateService.getStates())
      .subscribe(([states]) => {
        this.statesOptions = states;

        for (const state of states) {
          for (const city of state.cities) {
            if (city.id === Number(this.searchModel.cityId)) {
              this.searchModel.state = state;
            }
          }
        }

        orderCitiesOfState(this.statesOptions);

        this.searchSettingsReady = true;
      });

    this.resultsReady = true;
    if (this.searchModel.cityId !== null) {
      this.page = 1;
      // this.performSearch();
    }
  }

  performSearch() {
    this.resultsReady = false;
    this.tradesmanService.getTradesmen(this.searchModel, this.page).subscribe(response => {
      this.tradesmen = response.body;
      this.showNoResultsMessage = this.tradesmen.length <= 0;
      this.totalNumberOfResults = Number(response.headers.get('X-Total-Count'));
      this.resultsReady = true;
    });
  }

  stateSelectChange(state: any) {
    this.searchModel.state = state;
    this.searchModel.cityId = null;
  }

  areResultsReady() {
    return this.resultsReady;
  }

  onSubmit() {
    if (this.searchForm.form.valid) {
      this.totalNumberOfResults = null;
      this.page = 1;
      this.performSearch();
    }
  }

  pageChange(pageNumber: number) {
    this.performSearch();
  }

  onSearchByChanged(event) {
    Object.keys(this.searchModel).forEach(index => {
      this.searchModel[index] = null;
    });
  }
}
