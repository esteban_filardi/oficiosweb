interface TradesmanQuality {
  label: string
  sortOrder: number
}

export interface QualityScore {
  score: number
  tradesmanQuality: TradesmanQuality 
}

export interface Review {
  id: number,
  client: {
    firstName: string
    lastName: string
  },
  averageScore: number,
  qualityScores: QualityScore[]
  text: string
  createdDate : Date,
  anonymous: boolean
}
