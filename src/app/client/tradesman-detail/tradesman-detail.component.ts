import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TradesmanService } from '../../common/services/tradesman.service';
import { Tradesman } from '../../common/models/tradesman';
import { Location } from '@angular/common';
import { environment } from 'src/environments/environment';
import {ReviewService} from '../../common/services/review.service';
import {ClientAuthenticationService} from '../../common/services/auth/client-authentication.service';
import {Review} from './models/review';

@Component({
  selector: 'app-tradesman-detail',
  templateUrl: './tradesman-detail.component.html',
  styleUrls: ['./tradesman-detail.component.css']
})
export class TradesmanDetailComponent implements OnInit {
  tradesman: Tradesman;
  reviews: Review[];
  apiUrl: string;
  hasFinishedLoading = false;
  clientReviewId: string;

  constructor(
    private route: ActivatedRoute,
    private tradesmanService: TradesmanService,
    private clientAuthenticationService: ClientAuthenticationService,
    private reviewService: ReviewService,
    private location: Location
  ) {
    this.apiUrl = environment.apiUrl;
  }

  ngOnInit(): void {
    const tradesmanId = +this.route.snapshot.paramMap.get('id');

    this.tradesmanService.getTradesman(tradesmanId)
      .subscribe(tradesman => {
        this.tradesman = tradesman;
        this.hasFinishedLoading = true;
      });

    const loggedClient = this.clientAuthenticationService.getLoggedClient();

    this.reviewService.getTradesmanReviewsForClient(tradesmanId)
      .subscribe((reviews: Review[]) => {
        this.reviews = reviews;
      });

    if (loggedClient && loggedClient.clientId) {
      this.reviewService.getReviewsOfClient(loggedClient.clientId, tradesmanId)
        .subscribe(reviews => {
          if (reviews.length > 0) {
            this.clientReviewId = reviews[0].id;
          }
        });
    }
  }

  
}
