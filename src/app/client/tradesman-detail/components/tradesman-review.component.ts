import { Component, Input } from '@angular/core';
import { QualityScore, Review } from '../models/review';

@Component({
  selector: 'app-tradesman-review',
  templateUrl: './tradesman-review.component.html'
})
export class TradesmanReviewComponent {
  @Input("review")
  public review: Review;

  getReviewParagraphs(review: string): string[] {
    if (!review) {
      return [];
    }

    return review.replace(/\r/g, "").split(/\n/);
  }

  processQualityScores(qualityScores: QualityScore[]): QualityScore[]
  {
    var estimatedTermsQs = qualityScores.find(qs => qs.tradesmanQuality.label.includes(' estimados'));
    if (estimatedTermsQs?.tradesmanQuality?.label)
      estimatedTermsQs.tradesmanQuality.label = estimatedTermsQs.tradesmanQuality.label.replace(' estimados', '');

    return qualityScores.sort((a, b) => 
      a.tradesmanQuality.sortOrder <= b.tradesmanQuality.sortOrder ? -1 : 1
    );
  }
}
