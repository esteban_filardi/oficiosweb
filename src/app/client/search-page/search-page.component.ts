import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SkilledTrade } from '../../common/models/skilled-trade';
import { State } from '../../common/models/state';
import { Tradesman } from '../../common/models/tradesman';
import { SkilledTradeService } from '../../common/services/skilled-trade.service';
import { StateService } from '../../common/services/state.service';
import { TradesmanService } from '../../common/services/tradesman.service';
import { forkJoin } from 'rxjs';
import { environment } from 'src/environments/environment';
import {orderCitiesOfState} from '../../common/functions/order-cities-of-state';
import {SearchModel} from '../search-tradesman-by-personal-data/search-model';

@Component({
  templateUrl: './search-page.component.html'
})
export class SearchPageComponent implements OnInit {
  statesOptions: State[];
  skilledTradesOptions: SkilledTrade[];

  searchModel: SearchModel = {};

  tradesmen: Tradesman[] = [];

  searchSettingsReady = false;
  resultsReady = false;

  page: number;
  totalNumberOfResults: number;

  @ViewChild('searchForm') searchForm: { form: { valid: any; }; };
  apiUrl: string;

  constructor(
    private tradesmanService: TradesmanService,
    private skilledTradeService: SkilledTradeService,
    private stateService: StateService,
    private route: ActivatedRoute
  ) {
    this.apiUrl = environment.apiUrl;
  }

  ngOnInit(): void {
    this.searchModel.cityId = this.route.snapshot.queryParamMap.get('localidadId');
    this.searchModel.skilledTradeId =  this.route.snapshot.queryParamMap.get('oficioId');

    forkJoin(this.stateService.getStates(), this.skilledTradeService.getSkilledTrades())
      .subscribe(([states, skilledTrades]) => {
      this.statesOptions = states;

      for (const state of states) {
        for (const city of state.cities) {
          if (city.id === Number(this.searchModel.cityId)) {
            this.searchModel.state = state;
          }
        }
      }

      orderCitiesOfState(this.statesOptions);

      this.skilledTradesOptions = skilledTrades;
      this.searchSettingsReady = true;
    });


    if (this.searchModel.cityId !== null && this.searchModel.skilledTradeId !== null) {
      this.page = 1;
      this.performSearch();
    }
  }

  performSearch() {
    this.resultsReady = false;
    this.tradesmanService.getTradesmen(this.searchModel, this.page).subscribe(response => {
      this.tradesmen = response.body;
      this.totalNumberOfResults = Number(response.headers.get('X-Total-Count'));
      this.resultsReady = true;
    });
  }

  stateSelectChange(state: any) {
    this.searchModel.state = state;
    this.searchModel.cityId = null;
  }

  areResultsReady() {
    return this.resultsReady;
  }

  onSubmit() {
    if (this.searchForm.form.valid) {
      this.totalNumberOfResults = null;
      this.page = 1;
      this.performSearch();
    }
  }

  pageChange(pageNumber: number) {
    this.performSearch();
  }
}
