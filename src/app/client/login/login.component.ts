import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { LoginDataDto } from './login-data-dto';
import {ClientAuthenticationService} from '../../common/services/auth/client-authentication.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  returnUrl: string;
  message: string;
  @ViewChild('loginForm') loginForm: { form: { valid: any; }; };
  loginData: LoginDataDto = new LoginDataDto();
  submitting = false;
  invalidCredentials = false;
  clientIsBlocked = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private authService: ClientAuthenticationService
  ) { }

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
    this.message = this.route.snapshot.queryParamMap.get('message');
  }

  onLogin()  {
    if (!this.loginForm.form.valid) {
      return;
    }

    this.invalidCredentials = false;
    this.clientIsBlocked = false;
    this.submitting = true;
    const username = this.loginData.email;
    const password = this.loginData.password;

    this.authService.login(username, password)
      .subscribe(
        res => {
          this.submitting = false;
          console.log('Login successful.');
          console.log(res);
          this.redirectToPreviousPageOrHome();
        },
        (err: HttpErrorResponse) => {
          this.submitting = false;

          if (err.status === 401) {
            this.invalidCredentials = true;
            return;
          }

          if (err.status === 403 && err.error && err.error.type === 'userIsBlocked') {
            this.clientIsBlocked = true;
            return;
          }

          if (err.status === 403 && err.error && err.error.type === 'userIsNotEmailConfirmed') {
            this.router.navigate(['/usuario-no-verificado', {
              userType: 'client',
              userEmail: this.loginData.email
            }]);
            return;
          }

          console.error('Logging error', err);
          alert('Ha ocurrido un error. Por favor, intente luego nuevamente.');
        }
      );
  }

  showUserIsBlockedMessage() {
    this.invalidCredentials = false;
    this.clientIsBlocked = true;
  }

  facebookLoginSuccessful() {
    this.redirectToPreviousPageOrHome();
  }

  private redirectToPreviousPageOrHome() {
    this.router.navigate([this.returnUrl ? this.returnUrl : '/']);
  }
}
