import {Component, OnInit, NgZone, Input, EventEmitter, Output} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Router} from '@angular/router';

import {environment} from 'src/environments/environment';
import {AccessToken} from 'src/app/common/models/token-response';
import {ClientAuthenticationService} from '../../common/services/auth/client-authentication.service';
import {SocialAuthService, FacebookLoginProvider} from 'angularx-social-login';

@Component({
  selector: 'app-login-facebook',
  templateUrl: './login.facebook.component.html'
})
export class LoginFacebookComponent implements OnInit {
  baseUrl: string;
  @Input() returnUrl: string;
  @Output() userIsBlocked = new EventEmitter<void>();
  @Output() loginSuccessful = new EventEmitter<void>();


  constructor(
    private http: HttpClient,
    private router: Router,
    private SocialAuthService: ClientAuthenticationService,
    // inject the local zone
    private zone: NgZone,
    private socialSocialAuthService: SocialAuthService
  ) {
    this.baseUrl = environment.apiUrl;
  }

  ngOnInit(): void {
    this.socialSocialAuthService.authState.subscribe((user) => {
      console.log(user);
    });
  }

  signInWithFB(): void {
    this.socialSocialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID).then(user => {
        // call TokenController and register/login
        const url = this.baseUrl + 'facebook-tokens';
        const data = {
          facebookAccessToken: user.authToken,
        };
        this.http.post<AccessToken>(
          url, data)
          .subscribe(response => {
            if (response) {
              var tokenResponse = Object.assign(new AccessToken(), response);
              this.SocialAuthService.loginUsingFacebook(tokenResponse);
              this.loginSuccessful.emit();
            } else {
              console.log('Authentication failed');
            }
          }, (error: HttpErrorResponse) => {
            console.log(error);

            if (error.status === 403) {
              this.userIsBlocked.emit();
            }
          });
      }
    );
  }

  signOut(): void {
    this.socialSocialAuthService.signOut();
  }
}

