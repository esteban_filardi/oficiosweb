export interface Review {
  id: string;
  averageScore: number;
  text: string;
  createdDate: Date;
  clientId: string;
  clientName: string;
  tradesmanId: string;
  tradesmanName: string;
}
