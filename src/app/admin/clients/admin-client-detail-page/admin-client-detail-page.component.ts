import { Component, OnInit } from '@angular/core';
import {Client} from '../common/models/client';
import {ClientService} from '../../../common/services/client.service';
import {ActivatedRoute} from '@angular/router';
import {ReviewService} from '../../../common/services/review.service';

@Component({
  selector: 'app-admin-client-detail-page',
  templateUrl: './admin-client-detail-page.component.html',
  styleUrls: ['./admin-client-detail-page.component.scss']
})
export class AdminClientDetailPageComponent implements OnInit {
  client: Client;
  notification = null;
  reviews: any;

  constructor(
    private clientService: ClientService,
    private reviewService: ReviewService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    const clientId = this.route.snapshot.paramMap.get('clientId');

    this.clientService.getClient(clientId).subscribe(client => {
      this.client = client;
    });

    this.reviewService.getReviewsOfClient(clientId).subscribe(reviews => {
      this.reviews = reviews;
    });
  }

  deleteReview(id: string) {
    const confirmed = confirm('¿Esta seguro que desea que eliminar la opinión?');

    if (confirmed) {
      this.reviewService.deleteReview(id).subscribe(
        response => {
          for (let i = this.reviews.length - 1; i >= 0; --i) {
            if (this.reviews[i].id === id) {
              this.reviews.splice(i, 1);
            }
          }

          alert('La opinión ha sido eliminada.');
        }
      );
    }
  }

  blockUser() {
    this.clientService.blockClient(this.client.id).subscribe( () => {
        this.client.blocked = !this.client.blocked;
      }
    );
  }
}
