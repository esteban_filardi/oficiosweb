import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminClientDetailPageComponent } from './admin-client-detail-page.component';

describe('AdminClientDetailPageComponent', () => {
  let component: AdminClientDetailPageComponent;
  let fixture: ComponentFixture<AdminClientDetailPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminClientDetailPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminClientDetailPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
