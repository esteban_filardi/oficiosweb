export interface Client {
  id: number;
  firstName: string;
  lastName: string;
  createdDate: Date;
  email: string;
  blocked: boolean;
}
