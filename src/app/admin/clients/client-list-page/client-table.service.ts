import {Injectable} from '@angular/core';

import {BehaviorSubject, Observable, Subject} from 'rxjs';

import {environment} from '../../../../environments/environment';
import {debounceTime, delay, map, switchMap, tap} from 'rxjs/operators';
import {SortDirection} from '../../../common/directives/sortable.directive';
import {HttpClient} from '@angular/common/http';
import {Client} from '../common/models/client';
import {DatePipe} from '@angular/common';

interface SearchResult {
  clients: Client[];
  total: number;
}

interface State {
  page: number;
  pageSize: number;
  searchTerm: string;
  sortColumn: string;
  sortDirection: SortDirection;
}

function compare(v1, v2) {
  return v1 < v2 ? -1 : v1 > v2 ? 1 : 0;
}

function sort(clients: Client[], column: string, direction: string): Client[] {
  if (direction === '') {
    return clients;
  } else {
    return [...clients].sort((a, b) => {
      const res = column !== 'name' ? compare(a[column], b[column]) :
        compare(`${a.lastName}  ${a.firstName}`, `${b.lastName}  ${b.firstName}`);
      return direction === 'asc' ? res : -res;
    });
  }
}

function matches(client: Client, term: string, datePipe: DatePipe) {
  return client.firstName.toLowerCase().includes(term.toLowerCase())
    || client.lastName.toLowerCase().includes(term.toLowerCase())
    || client.email.toLowerCase().includes(term.toLowerCase())
    || datePipe.transform(client.createdDate, 'd/M/yyyy').includes(term.toLowerCase());
}

@Injectable({providedIn: 'root'})
export class ClientTableService {
  private _loading$ = new BehaviorSubject<boolean>(true);
  private _search$ = new Subject<void>();
  private _clients$ = new BehaviorSubject<Client[]>([]);
  private _total$ = new BehaviorSubject<number>(0);

  private _state: State = {
    page: 1,
    pageSize: 10,
    searchTerm: '',
    sortColumn: '',
    sortDirection: ''
  };

  constructor(private httpClient: HttpClient,
              private datePipe: DatePipe) {
    this._search$.pipe(
      tap(() => this._loading$.next(true)),
      debounceTime(200),
      switchMap(() => this._search()),
      delay(0),
      tap(() => this._loading$.next(false))
    ).subscribe(result => {
      this._clients$.next(result.clients);
      this._total$.next(result.total);
    });

    this._search$.next();
  }

  get clients$() {
    return this._clients$.asObservable();
  }

  get total$() {
    return this._total$.asObservable();
  }

  get loading$() {
    return this._loading$.asObservable();
  }

  get page() {
    return this._state.page;
  }

  set page(page: number) {
    this._set({page});
  }

  get pageSize() {
    return this._state.pageSize;
  }

  set pageSize(pageSize: number) {
    this._set({pageSize});
  }

  get searchTerm() {
    return this._state.searchTerm;
  }

  set searchTerm(searchTerm: string) {
    this._set({searchTerm});
  }

  set sortColumn(sortColumn: string) {
    this._set({sortColumn});
  }

  set sortDirection(sortDirection: SortDirection) {
    this._set({sortDirection});
  }

  private _set(patch: Partial<State>) {
    Object.assign(this._state, patch);
    this._search$.next();
  }

  private _search(): Observable<SearchResult> {
    const {sortColumn, sortDirection, pageSize, page, searchTerm} = this._state;

    return this.getClients().pipe(map((clients) => {
      // 1. sort
      let sortedClients = sort(clients, sortColumn, sortDirection);

      // 2. filter
      sortedClients = sortedClients.filter(client => matches(client, searchTerm, this.datePipe));
      const total = sortedClients.length;

      // 3. paginate
      sortedClients = sortedClients.slice((page - 1) * pageSize, (page - 1) * pageSize + pageSize);
      return {clients: sortedClients, total};
    }));
  }

  getClients(): Observable<Client[]> {
    const methodUrl = `${environment.apiUrl}clients`;
    return map((response: Client[]) => {
      return response;
    })(this.httpClient.get<Client[]>((methodUrl)));
  }
}

