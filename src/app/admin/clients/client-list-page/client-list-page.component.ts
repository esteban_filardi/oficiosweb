import {Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Client} from '../common/models/client';
import {ClientTableService} from './client-table.service';
import {Observable} from 'rxjs';
import {Tradesman} from '../../common/models/tradesman';
import {NgbdSortableHeaderDirective, SortEvent} from '../../../common/directives/sortable.directive';

@Component({
  selector: 'app-client-list-page',
  templateUrl: './client-list-page.component.html',
  styleUrls: ['./client-list-page.component.scss']
})
export class ClientListPageComponent implements OnInit {
  clients$: Observable<Client[]>;
  total$: Observable<number>;
  @ViewChildren(NgbdSortableHeaderDirective) headers: QueryList<NgbdSortableHeaderDirective>;
  notification = null;

  constructor(
    public service: ClientTableService,
    private route: ActivatedRoute
  ) {
    this.clients$ = service.clients$;
    this.total$ = service.total$;
  }

  ngOnInit() {
    if (this.route.snapshot.paramMap.get('notification_type')) {
      this.notification = {
        type: this.route.snapshot.paramMap.get('notification_type'),
        message: this.route.snapshot.paramMap.get('notification_message'),
      };
    }
  }

  onSort({column, direction}: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.service.sortColumn = column;
    this.service.sortDirection = direction;
  }
}
