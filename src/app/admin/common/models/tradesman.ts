export interface Tradesman {
  id: string;
  firstName: string;
  lastName: string;
  cityAndState: string;
  createdDate: Date;
  blocked: boolean;
}
