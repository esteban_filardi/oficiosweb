import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {SkilledTrade} from '../../skilled-trades/admin-trades/models/skilled-trade';

@Injectable({
  providedIn: 'root'
})
export class TradesService {

  apiUrl = environment.apiUrl;

  constructor(private httpClient: HttpClient) {}

  getTrades(): Observable<SkilledTrade[]> {
    const methodUrl = `${this.apiUrl}skilledTrades`;
    return map((response: SkilledTrade[]) => {
      console.log(response);
      return response;
    })(this.httpClient.get<SkilledTrade[]>((methodUrl)));
  }

  createSkilledTrade(name: string) {
    const methodUrl = `${this.apiUrl}skilledTrades`;

    return map((response: SkilledTrade[]) => {
      console.log(response);
      return response;
    })(this.httpClient.post<SkilledTrade[]>(methodUrl, {
      name
    }));
  }

  getSkilledTrade(id: number) {
    const methodUrl = `${this.apiUrl}skilledTrades/${id}`;
    return map((response: SkilledTrade) => {
      console.log(response);
      return response;
    })(this.httpClient.get<SkilledTrade>((methodUrl)));
  }

  deleteSkilledTrade(id: number) {
    const methodUrl = `${this.apiUrl}skilledTrades/${id}`;
    return map((response) => {
      return response;
    })(this.httpClient.delete(methodUrl));
  }

  updateSkilledTrade(id: number, name: string) {
    const methodUrl = `${this.apiUrl}skilledTrades/${id}`;
    return map((response) => {
      return response;
    })(this.httpClient.put(methodUrl, {name}));
  }
}
