import { Injectable } from '@angular/core';
import { Observable, Subject, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { AccessToken } from '../../../common/models/token-response';
import { catchError, map } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/common/services/auth/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AdminAutenticationService {
  readonly clientAuthKey = 'client-auth';
  readonly tradesmanAuthKey = 'tradesman-auth';
  readonly adminAuthKey = 'admin-auth';
  readonly clientId = 'Oficios';

  private loginSource = new Subject<any>();
  private logoutSource = new Subject<any>();

  loginSource$ = this.loginSource.asObservable();
  logoutSource$ = this.logoutSource.asObservable();

  constructor(
    private authenticationService: AuthenticationService,
    private httpClient: HttpClient
  ) {
  }

  login(username: string, password: string): Observable<boolean> {
    const url = environment.apiUrl + 'tokens';
    const data = {
      username,
      password,
      role: "Admin"
    };

    return this.httpClient.post<AccessToken>(url, data)
      .pipe(map(response => {
        var tokenResponse = Object.assign(new AccessToken(), response);

        // store username and jwt token
        this.authenticationService.storeAccessToken(response.accessToken);
        this.setAuth({
          accessToken: tokenResponse.accessToken
        });

        this.removeOtherAuthorizationData();

        // successful login
        this.loginSource.next(response);
        return true;

      }), catchError(error => {
        console.log(error);
        return throwError(false);
      }));
  }

  setAuth(auth: any): boolean {
    if (auth) {
      localStorage.setItem(
        this.adminAuthKey,
        JSON.stringify(auth));
    } else {
      localStorage.removeItem(this.adminAuthKey);
    }

    return true;
  }

  loginUsingFacebook(loginData: any) {
    this.setAuth(loginData);
    this.loginSource.next(loginData);
  }

  logout() {
    localStorage.removeItem((this.adminAuthKey));
    this.loginSource.next();
  }

  getLoggedClient() {
    return JSON.parse(localStorage.getItem(this.adminAuthKey));
  }

  private removeOtherAuthorizationData() {
    localStorage.removeItem(this.clientAuthKey);
    localStorage.removeItem(this.tradesmanAuthKey);
  }
}
