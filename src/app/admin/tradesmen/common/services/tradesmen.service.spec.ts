import { TestBed } from '@angular/core/testing';

import { TradesmenService } from './tradesmen.service';

describe('TradesmenService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TradesmenService = TestBed.get(TradesmenService);
    expect(service).toBeTruthy();
  });
});
