import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Tradesman} from '../../../../common/models/tradesman';
import {environment} from '../../../../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TradesmenService {

  constructor(
    private httpClient: HttpClient) {
  }

  getTradesman(id: string): Observable<Tradesman> {
    return this.httpClient.get<Tradesman>(environment.apiUrl + 'tradesmen/' + id);
  }

  getTradesmanByUserId(userId: string): Observable<Tradesman> {
    const methodUrl = `${environment.apiUrl}tradesmen/plural`;

    return this.httpClient.get<Tradesman[]>(methodUrl, {
      params: {
        userId
      }
    }).pipe(map(tradesmen => {
      return tradesmen.length > 0 ? tradesmen[0] : null;
    }));
  }

  blockClient(tradesmanId: number): Observable<void> {
    const methodUrl = `${environment.apiUrl}tradesmen/${tradesmanId}/blocked`;

    return this.httpClient.put<void>(methodUrl, {});
  }
}
