import {Injectable} from '@angular/core';

import {BehaviorSubject, Observable, of, Subject} from 'rxjs';

import {environment} from '../../../../environments/environment';
import {debounceTime, delay, map, switchMap, tap} from 'rxjs/operators';
import {SortDirection} from '../../../common/directives/sortable.directive';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Tradesman} from '../../common/models/tradesman';
import {DatePipe} from '@angular/common';

interface SearchResult {
  tradesmen: Tradesman[];
  total: number;
}

interface State {
  page: number;
  pageSize: number;
  searchTerm: string;
  sortColumn: string;
  sortDirection: SortDirection;
}

function compare(v1, v2) {
  return v1 < v2 ? -1 : v1 > v2 ? 1 : 0;
}

function sort(tradesmen: Tradesman[], column: string, direction: string): Tradesman[] {
  if (direction === '') {
    return tradesmen;
  } else {
    return [...tradesmen].sort((a, b) => {
      const res = column !== 'name' ? compare(a[column], b[column]) :
        compare(`${a.lastName}  ${a.firstName}`, `${b.lastName}  ${b.firstName}`);
      return direction === 'asc' ? res : -res;
    });
  }
}

function matches(tradesman: Tradesman, term: string, datePipe: DatePipe) {
  return tradesman.firstName.toLowerCase().includes(term.toLowerCase())
    || tradesman.lastName.toLowerCase().includes(term.toLowerCase())
    || tradesman.cityAndState.toLowerCase().includes(term.toLowerCase())
    || datePipe.transform(tradesman.createdDate, 'd/M/yyyy').includes(term.toLowerCase());
}

@Injectable({providedIn: 'root'})
export class TradesmenTableService {
  private _loading$ = new BehaviorSubject<boolean>(true);
  private _search$ = new Subject<void>();
  private _tradesmen$ = new BehaviorSubject<Tradesman[]>([]);
  private _total$ = new BehaviorSubject<number>(0);

  private _state: State = {
    page: 1,
    pageSize: 10,
    searchTerm: '',
    sortColumn: '',
    sortDirection: ''
  };

  constructor(private httpClient: HttpClient,
              private datePipe: DatePipe) {
    this._search$.pipe(
      tap(() => this._loading$.next(true)),
      debounceTime(200),
      switchMap(() => this._search()),
      delay(0),
      tap(() => this._loading$.next(false))
    ).subscribe(result => {
      this._tradesmen$.next(result.tradesmen);
      this._total$.next(result.total);
    });

    this._search$.next();
  }

  get tradesmen$() {
    return this._tradesmen$.asObservable();
  }

  get total$() {
    return this._total$.asObservable();
  }

  get loading$() {
    return this._loading$.asObservable();
  }

  get page() {
    return this._state.page;
  }

  set page(page: number) {
    this._set({page});
  }

  get pageSize() {
    return this._state.pageSize;
  }

  set pageSize(pageSize: number) {
    this._set({pageSize});
  }

  get searchTerm() {
    return this._state.searchTerm;
  }

  set searchTerm(searchTerm: string) {
    this._set({searchTerm});
  }

  set sortColumn(sortColumn: string) {
    this._set({sortColumn});
  }

  set sortDirection(sortDirection: SortDirection) {
    this._set({sortDirection});
  }

  private _set(patch: Partial<State>) {
    Object.assign(this._state, patch);
    this._search$.next();
  }

  private _search(): Observable<SearchResult> {
    const {sortColumn, sortDirection, pageSize, page, searchTerm} = this._state;

    return this.getTradesmen().pipe(map((tradesmen) => {
      // 1. sort
      let sortedTradesmen = sort(tradesmen, sortColumn, sortDirection);

      // 2. filter
      sortedTradesmen = sortedTradesmen.filter(tradesman => matches(tradesman, searchTerm, this.datePipe));
      const total = sortedTradesmen.length;

      // 3. paginate
      sortedTradesmen = sortedTradesmen.slice((page - 1) * pageSize, (page - 1) * pageSize + pageSize);
      return {tradesmen: sortedTradesmen, total};
    }));
  }

  getTradesmen(page = 1): Observable<Tradesman[]> {
    return this.httpClient.get<Tradesman[]>(environment.apiUrl + 'tradesmen/for-admin', {
      observe: 'response',
      params: {
        page: String(page)
      }
    }).pipe(map(response => {
      return response.body;
    }));
  }
}

