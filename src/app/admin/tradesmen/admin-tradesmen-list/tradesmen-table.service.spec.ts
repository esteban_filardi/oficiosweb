import { TestBed } from '@angular/core/testing';

import { TradesmenTableService } from './tradesmen-table.service';

describe('TradesmenTableService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TradesmenTableService = TestBed.get(TradesmenTableService);
    expect(service).toBeTruthy();
  });
});
