import {Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Tradesman} from '../../common/models/tradesman';
import {TradesmenTableService} from './tradesmen-table.service';
import {NgbdSortableHeaderDirective, SortEvent} from '../../../common/directives/sortable.directive';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-admin-tradesmen-list',
  templateUrl: './admin-tradesmen-list.component.html',
  styleUrls: ['./admin-tradesmen-list.component.scss']
})
export class AdminTradesmenListComponent implements OnInit {
  tradesmen$: Observable<Tradesman[]>;
  total$: Observable<number>;
  notification = null;

  @ViewChildren(NgbdSortableHeaderDirective) headers: QueryList<NgbdSortableHeaderDirective>;

  constructor(public service: TradesmenTableService,
              private route: ActivatedRoute
  ) {
    this.tradesmen$ = service.tradesmen$;
    this.total$ = service.total$;
  }

  onSort({column, direction}: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.service.sortColumn = column;
    this.service.sortDirection = direction;
  }

  ngOnInit() {
    if (this.route.snapshot.paramMap.get('notification_type')) {
      this.notification = {
        type: this.route.snapshot.paramMap.get('notification_type'),
        message: this.route.snapshot.paramMap.get('notification_message'),
      };
    }
  }
}
