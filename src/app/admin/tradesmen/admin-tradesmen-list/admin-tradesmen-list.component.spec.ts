import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminTradesmenListComponent } from './admin-tradesmen-list.component';

describe('AdminTradesmenListComponent', () => {
  let component: AdminTradesmenListComponent;
  let fixture: ComponentFixture<AdminTradesmenListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminTradesmenListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminTradesmenListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
