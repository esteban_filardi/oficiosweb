import { Component, OnInit } from '@angular/core';
import {TradesmenService} from '../common/services/tradesmen.service';
import {ActivatedRoute} from '@angular/router';
import {ReviewService} from '../../../common/services/review.service';
import {Tradesman} from '../../../common/models/tradesman';

@Component({
  selector: 'app-admin-tradesman-detail',
  templateUrl: './admin-tradesman-detail.component.html',
  styleUrls: ['./admin-tradesman-detail.component.scss']
})
export class AdminTradesmanDetailComponent implements OnInit {
  tradesman: Tradesman;
  notification = null;
  reviews: any;

  constructor(
    private tradesmenService: TradesmenService,
    private reviewService: ReviewService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    const tradesmanId = this.route.snapshot.paramMap.get('tradesmanId');

    this.tradesmenService.getTradesman(tradesmanId).subscribe(tradesman => {
      this.tradesman = tradesman;
      this.reviews = tradesman.reviews;
    });
  }

  deleteReview(id: string) {
    const confirmed = confirm('¿Esta seguro que desea que eliminar la opinión?');

    if (confirmed) {
      this.reviewService.deleteReview(id).subscribe(
        response => {
          for (let i = this.reviews.length - 1; i >= 0; --i) {
            if (this.reviews[i].id === id) {
              this.reviews.splice(i, 1);
            }
          }

          alert('La opinión ha sido eliminada.');
        }
      );
    }
  }

  blockUser() {
    this.tradesmenService.blockClient(this.tradesman.id).subscribe( () => {
        this.tradesman.blocked = !this.tradesman.blocked;
      }
    );
  }
}
