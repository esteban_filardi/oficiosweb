import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminTradesmanDetailComponent } from './admin-tradesman-detail.component';

describe('AdminTradesmanDetailComponent', () => {
  let component: AdminTradesmanDetailComponent;
  let fixture: ComponentFixture<AdminTradesmanDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminTradesmanDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminTradesmanDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
