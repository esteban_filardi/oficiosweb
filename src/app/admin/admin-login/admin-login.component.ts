import {Component, OnInit, ViewChild} from '@angular/core';
import {LoginDataDto} from '../../tradesman/login/login-data-dto';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {AdminAutenticationService} from '../common/services/admin-autentication.service';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.scss']
})
export class AdminLoginComponent implements OnInit {
  @ViewChild('loginForm') loginForm: { form: { valid: any; }; };
  loginData: LoginDataDto = new LoginDataDto();
  submitting = false;
  invalidCredentials = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private authService: AdminAutenticationService
  ) {}

  ngOnInit() {}

  onLogin() {
    if (!this.loginForm.form.valid) {
      return;
    }

    this.invalidCredentials = false;
    this.submitting = true;
    const username = this.loginData.email;
    const password = this.loginData.password;

    this.authService.login(username, password)
      .subscribe(
        res => {
          console.log('Logging success');
          this.submitting = false;
          this.router.navigate(['/area-administrador']);
        },
        err => {
          console.log('Logging error');
          // login failed
          console.log(err);
          this.invalidCredentials = true;
          this.submitting = false;
        }
      );
  }
}
