import { Component, OnInit } from '@angular/core';
import {SkilledTrade} from '../admin-trades/models/skilled-trade';
import {TradesService} from '../../common/services/trades.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-admin-skilled-trade-edit',
  templateUrl: './admin-skilled-trade-edit.component.html',
  styleUrls: ['./admin-skilled-trade-edit.component.scss']
})
export class AdminSkilledTradeEditComponent implements OnInit {
  model: SkilledTrade;
  submitting = false;

  constructor(private tradeService: TradesService,
              private router: Router,
              private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.tradeService.getSkilledTrade(+this.route.snapshot.paramMap.get('id'))
      .subscribe(response => {
        this.model = response;
      });
  }

  onSubmit() {
    this.submitting = true;

    this.tradeService.updateSkilledTrade(this.model.id, this.model.name)
      .subscribe(response => {
        this.router.navigate(['/area-administrador/oficios',
          {notification_type: 'success', notification_message: 'El oficio ha sido actualizado.'}], {
        });
        this.submitting = false;
      });
  }
}
