import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminSkilledTradeEditComponent } from './admin-skilled-trade-edit.component';

describe('AdminSkilledTradeEditComponent', () => {
  let component: AdminSkilledTradeEditComponent;
  let fixture: ComponentFixture<AdminSkilledTradeEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminSkilledTradeEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminSkilledTradeEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
