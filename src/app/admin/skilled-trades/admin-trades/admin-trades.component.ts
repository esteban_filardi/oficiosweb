import { Component, OnInit } from '@angular/core';
import {TradesService} from '../../common/services/trades.service';
import {SkilledTrade} from './models/skilled-trade';
import {ActivatedRoute} from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-admin-trades',
  templateUrl: './admin-trades.component.html',
  styleUrls: ['./admin-trades.component.scss']
})
export class AdminTradesComponent implements OnInit {

  skilledTrades: SkilledTrade[];
  notification = null;

  constructor(
    private tradesService: TradesService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    if (this.route.snapshot.paramMap.get('notification_type')) {
      this.notification = {
        type: this.route.snapshot.paramMap.get('notification_type'),
        message: this.route.snapshot.paramMap.get('notification_message'),
      };
    }

    this.tradesService.getTrades().subscribe(response => {
      this.skilledTrades = response;
    });
  }

  deleteSkilledTrade(id: number) {
    const confirmed = confirm('¿Esta seguro que desea que eliminar el oficio?');

    if (confirmed) {
      this.tradesService.deleteSkilledTrade(id).subscribe(
        response => {
          for (let i = this.skilledTrades.length - 1; i >= 0; --i) {
            if (this.skilledTrades[i].id === id) {
              this.skilledTrades.splice(i, 1);
            }
          }
        }
        , (error) => {
          console.error(error);
          if (error instanceof HttpErrorResponse) {
            if (error.status === 409) {
              alert('No se puede eliminar el oficio debido a que trabajadores se encuentran asignados al mismo.');
            }
          }
        });
    }
  }
}
