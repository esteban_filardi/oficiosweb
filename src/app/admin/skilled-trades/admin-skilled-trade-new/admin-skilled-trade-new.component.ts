import { Component, OnInit } from '@angular/core';
import {SkilledTrade} from '../admin-trades/models/skilled-trade';
import {TradesService} from '../../common/services/trades.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-admin-skilled-trade-new',
  templateUrl: './admin-skilled-trade-new.component.html',
  styleUrls: ['./admin-skilled-trade-new.component.scss']
})
export class AdminSkilledTradeNewComponent {
  model: SkilledTrade = {
    id: null,
    name: ''
  };
  private submitting = false;

  constructor(private tradeService: TradesService, private router: Router) { }

  onSubmit() {
    this.tradeService.createSkilledTrade(this.model.name)
      .subscribe(response => {
        this.router.navigate(['/area-administrador/oficios',
          {notification_type: 'success', notification_message: 'El oficio ha sido creado.'}], {
        });
        this.submitting = false;
      });
  }

}

