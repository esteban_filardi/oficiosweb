import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminSkilledTradeNewComponent } from './admin-skilled-trade-new.component';

describe('AdminSkilledTradeNewComponent', () => {
  let component: AdminSkilledTradeNewComponent;
  let fixture: ComponentFixture<AdminSkilledTradeNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminSkilledTradeNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminSkilledTradeNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
