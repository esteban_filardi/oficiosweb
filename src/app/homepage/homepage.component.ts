import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { StateService } from '../common/services/state.service';
import { State } from '../common/models/state';
import { SkilledTrade } from '../common/models/skilled-trade';
import { SkilledTradeService } from '../common/services/skilled-trade.service';
import {orderCitiesOfState} from '../common/functions/order-cities-of-state';
import { forkJoin } from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';

@Component({
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {
  states: State[];
  skilledTrades: SkilledTrade[];
  hasFinishedLoading = false;
  @ViewChild('searchForm') searchForm;
  model: any = {
    skilledTradeId: null,
    state: null,
    cityId: null
  };

  constructor(
    private skilledTradeService: SkilledTradeService,
    private stateService: StateService,
    private httpClient: HttpClient,
    private router: Router) { }

  ngOnInit() {
    forkJoin(
      this.stateService.getStates(),
      this.skilledTradeService.getSkilledTrades()
    ).subscribe(([states, skilledTrades]) => {
      this.states = states;
      orderCitiesOfState(this.states);
      this.skilledTrades = skilledTrades;

      this.setAutomaticallyDetectedCity().subscribe(() => {
        this.hasFinishedLoading = true;
      }, (error) => {
        console.error('Error while detecting city:', error);
        this.hasFinishedLoading = true;
      });
    });
  }

  stateSelectChange(state: any) {
    this.model.state = state;
    this.model.cityId = null;
  }

  onSubmit() {
    if (this.searchForm.form.valid) {
      const queryParams = {
        oficioId: this.model.skilledTradeId,
        localidadId: this.model.cityId
      };

      this.updateQueryParameters(queryParams).then(() => {
        this.navigateToSearchPage(queryParams);
      });
    }
  }

  updateQueryParameters(queryParams): Promise<boolean> {
    return this.router.navigate(['.'], {
      replaceUrl: true,
      queryParams
    });
  }

  navigateToSearchPage(queryParams): Promise<boolean> {
    return this.router.navigate(['/busqueda'], {
      queryParams
    });
  }

  private setAutomaticallyDetectedCity() {
    return this.httpClient.jsonp('https://extreme-ip-lookup.com/json/', 'callback').pipe(map((locationInfo: any) => {

      for (const state of this.states) {
        if (state.name === locationInfo.region) {
          this.stateSelectChange(state);

          for (const city of state.cities) {
            if (city.name === locationInfo.city) {
              this.model.cityId = city.id;
              return true;
            }
          }

          return true;
        }
      }
    }));
  }
}
