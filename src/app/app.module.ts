import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClientJsonpModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { TextMaskModule } from 'angular2-text-mask';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { SocialLoginModule, SocialAuthServiceConfig  } from 'angularx-social-login';
import { FacebookLoginProvider } from 'angularx-social-login';

import {
  faHome,
  faEnvelope,
  faMobileAlt,
  faEye,
  faStar,
  faTools,
  faUsers,
  faUsersCog,
} from '@fortawesome/free-solid-svg-icons';
import {faFacebookF} from '@fortawesome/free-brands-svg-icons';

import locale from '@angular/common/locales/es-AR';
import localeExtra from '@angular/common/locales/extra/es-AR';
import { registerLocaleData } from '@angular/common';
registerLocaleData(locale, 'es-AR', localeExtra);

import { AppRoutingModule } from './app-routing.module';

import {AddTokenToRequestInterceptor} from './common/services/auth/add-token-to-request.interceptor';

import { AppComponent } from './app.component';
import {NavbarComponent} from './common/navbar/navbar.component';
import {HomepageComponent} from './homepage/homepage.component';
import {SearchPageComponent} from './client/search-page/search-page.component';
import { TradesmanRegistrationComponent } from './tradesman/tradesman-registration/tradesman-registration.component';
import { UserConfirmAccountComponent } from './common/components/user-confirm-account/user-confirm-account.component';
import { TradesmanDetailComponent } from './client/tradesman-detail/tradesman-detail.component';
import { TradesmanReviewComponent } from './client/tradesman-detail/components/tradesman-review.component';
import { WriteReviewComponent } from './client/write-review/write-review.component';
import { LoginComponent } from './client/login/login.component';
import { ConfirmEqualValidatorDirective } from './common/directives/confirm-equal-validator.directive';
import { LoginFacebookComponent } from './client/login/login-facebook.component';
import { ClientRegistrationComponent } from './client/client-registration/client-registration.component';
import { SpinnerComponent } from './common/components/spinner/spinner.component';
import { LoginComponent as TradesmanLoginComponent } from './tradesman/login/login.component';
import { TradesmanDashboardComponent } from './tradesman/tradesman-dashboard/tradesman-dashboard.component';
import { SiteLayoutComponent } from './common/components/site-layout/site-layout.component';
import { TradesmanAreaLayoutComponent } from './common/components/tradesman-area-layout/tradesman-area-layout.component';
import { TradesmanProfileComponent } from './tradesman/tradesman-profile/tradesman-profile.component';
import { TradesmanProfileEditComponent } from './tradesman/tradesman-profile-edit/tradesman-profile-edit.component';
import { TradesmanReviewsComponent } from './tradesman/tradesman-reviews/tradesman-reviews.component';
import { AdminLoginComponent } from './admin/admin-login/admin-login.component';
import { AdminDashboardComponent } from './admin/common/components/admin-dashboard/admin-dashboard.component';
import { AdminLayoutComponent } from './admin/common/components/admin-layout/admin-layout.component';
import { AdminTradesComponent } from './admin/skilled-trades/admin-trades/admin-trades.component';
import { AdminSkilledTradeEditComponent } from './admin/skilled-trades/admin-skilled-trade-edit/admin-skilled-trade-edit.component';
import { AdminSkilledTradeNewComponent } from './admin/skilled-trades/admin-skilled-trade-new/admin-skilled-trade-new.component';
import { ClientListPageComponent } from './admin/clients/client-list-page/client-list-page.component';
import { AdminClientDetailPageComponent } from './admin/clients/admin-client-detail-page/admin-client-detail-page.component';
import { AdminTradesmenListComponent } from './admin/tradesmen/admin-tradesmen-list/admin-tradesmen-list.component';
import { AdminTradesmanDetailComponent } from './admin/tradesmen/admin-tradesman-detail/admin-tradesman-detail.component';
import {NgbdSortableHeaderDirective} from './common/directives/sortable.directive';
import {DatePipe} from '@angular/common';
import { SearchTradesmanByPersonalDataComponent } from './client/search-tradesman-by-personal-data/search-tradesman-by-personal-data.component';
import { UserNotEmailConfirmedComponent } from './common/components/user-not-email-confirmed/user-not-email-confirmed.component';
import { UpdateReviewComponent } from './client/update-review/update-review.component';

import {environment} from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    SiteLayoutComponent,
    TradesmanAreaLayoutComponent,
    NavbarComponent,
    HomepageComponent,
    SearchPageComponent,
    LoginComponent,
    ClientRegistrationComponent,
    TradesmanRegistrationComponent,
    UserConfirmAccountComponent,
    TradesmanDetailComponent,
    TradesmanReviewComponent,
    WriteReviewComponent,
    ConfirmEqualValidatorDirective,
    LoginFacebookComponent,
    TradesmanLoginComponent,
    TradesmanDashboardComponent,
    TradesmanProfileComponent,
    TradesmanProfileEditComponent,
    SpinnerComponent,
    TradesmanReviewsComponent,
    AdminLoginComponent,
    AdminDashboardComponent,
    AdminLayoutComponent,
    AdminTradesComponent,
    AdminSkilledTradeEditComponent,
    AdminSkilledTradeNewComponent,
    ClientListPageComponent,
    AdminClientDetailPageComponent,
    AdminTradesmenListComponent,
    AdminTradesmanDetailComponent,
    NgbdSortableHeaderDirective,
    SearchTradesmanByPersonalDataComponent,
    UserNotEmailConfirmedComponent,
    UpdateReviewComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    HttpClientJsonpModule,
    NgbModule,
    NgSelectModule,
    TextMaskModule,
    CKEditorModule,
    FontAwesomeModule,
    SocialLoginModule
  ],
  providers: [
    DatePipe,
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider(environment.facebookAppId)
          }
        ],
      } as SocialAuthServiceConfig
    },
    { provide: HTTP_INTERCEPTORS, useClass: AddTokenToRequestInterceptor, multi: true },
    { provide: LOCALE_ID, useValue: 'es-AR'},
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(library: FaIconLibrary) {
    // Add an icon to the library for convenient access in other components
    library.addIcons(faHome,
      faEnvelope,
      faMobileAlt,
      faEye,
      faTools,
      faStar,
      faUsers,
      faUsersCog,
      faFacebookF);
  }
}
